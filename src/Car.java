/**
 * Roller Coaster problem using Locks and Conditions
 * 
 * Car class, this thread wait for boarding people, do the trip, unload people
 * and so on indefinitly.
 * 
 * @author Kevin Python <kevin.python@hefr.ch>
 * @version 1.0
 * @since 2012-05-16
 * 
 */

public class Car extends Thread {

   final static int ROUND_TIME  = 5000;
   static int       NB_OF_SEATS = 6;

   public void run() {
	  while (true) {
		 try {
			RollerCoaster.monitor.load();
			Thread.sleep(ROUND_TIME);
			RollerCoaster.monitor.unload();
		 }

		 catch (InterruptedException e) {
			e.printStackTrace();
		 }
	  }
   }
}
