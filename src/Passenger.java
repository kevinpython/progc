/**
 * Roller Coaster problem using Locks and Conditions
 * 
 * Passanger Class, this thread represent a client who wait the car, sits in the
 * car, do randoms animations, wait the and of the trip and leave the car and so
 * on indefinitly.
 * 
 * @author Kevin Python <kevin.python@hefr.ch>
 * @version 1.0
 * @since 2012-05-16
 * 
 */

public class Passenger extends Thread {

   private String   name;

   static String[]  animations            = { " raises his hands", " vomits",
	     " says \"Youuhouu !\"", " cries", " is scared !", " laughs !" };

   static final int MAX_WAIT_TIME         = 30000;
   static final int MIN_WAIT_TIME         = 20000;
   static final int MAX_WAIT_TIME_FIRST   = 10000;
   static final int MIN_WAIT_TIME_FISRT   = 1000;
   static final int MAX_WAIT_TIME_ANIMATE = 2000;
   static final int MIN_WAIT_TIME_ANIMATE = 1000;

   public Passenger(String name) {
	  this.name = name;
   }

   public void run() {
	  try {
		 // Time to decide to board for the first time.
		 Thread.sleep(MIN_WAIT_TIME_FISRT
			   + RollerCoaster.random.nextInt(MAX_WAIT_TIME_FIRST
			         - MIN_WAIT_TIME_FISRT));

		 while (true) {
			RollerCoaster.monitor.board(name);
			RollerCoaster.monitor.animate(name);
			RollerCoaster.monitor.unboard(name);

			// Time to decide to board again.
			Thread.sleep(MIN_WAIT_TIME
			      + RollerCoaster.random.nextInt(MAX_WAIT_TIME - MIN_WAIT_TIME));
		 }
	  } catch (InterruptedException e1) {
		 e1.printStackTrace();
	  }
   }
}
