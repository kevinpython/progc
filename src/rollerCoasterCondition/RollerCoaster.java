package rollerCoasterCondition;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class RollerCoaster {

   public static Monitor monitor = new Monitor(); 
   public static ExecutorService threadExecutor = Executors.newCachedThreadPool();
   final static int NB_OF_PASSAGER = 8;
   
   
   public static void main(String[] args) {
	  String name;
      File inFile = new File("names.txt");
      Scanner in;
      try {
	     in = new Scanner(inFile);
	     Car car = new Car();
	      Passager passager[] = new Passager[NB_OF_PASSAGER];
	      threadExecutor.execute(car);
	      for (int i = 0; i < NB_OF_PASSAGER; i++) {
	          name = in.nextLine();
	          passager[i] = new Passager(name);
	          threadExecutor.execute(passager[i]);
	      }
      } catch (FileNotFoundException e) {
	     e.printStackTrace();
      }
   }
}
