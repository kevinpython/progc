package rollerCoasterCondition;

import java.util.Random;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class Monitor {

   Random          random            = new Random();

   final Lock      lock              = new ReentrantLock();

   // The car wait that the car is full before running.
   final Condition car_full          = lock.newCondition();
   // Queue for waiting people who wants to board.
   final Condition car_free          = lock.newCondition();
   // Queue for people in the car, they're waiting the end of the run.
   final Condition run_done          = lock.newCondition();
   // Queue for people ho are waiting the start of the run before animate.
   final Condition car_started		 = lock.newCondition();

   boolean         passagersCanBoard = false;
   int             passager_on_board = 0;

   //
   // Methods called by the passager
   //
   public void board(String name) throws InterruptedException {
	  lock.lock();
	  try {

		 System.out.println("The passenger " + name + " wants to board");
		 while (!passagersCanBoard) {
			System.out.println("The passenger " + name + " has to wait !");
			car_free.await();
		 }

		 passager_on_board++;
		 System.out.println("> The passenger (nb " + passager_on_board + ") "
			   + name + " sits in the car");
		 if (passager_on_board == Car.NB_OF_SEATS) {
			car_full.signal();
			passagersCanBoard = false;
		 }
		 
		 car_started.await();

	  } finally {
		 lock.unlock();
	  }
   }

   public void unboard(String name) throws InterruptedException {
	  lock.lock();
	  try {
		 run_done.await();
		 passager_on_board--;
		 if (passager_on_board == 0) {
			car_free.signalAll();
		 }
		 System.out.println("< The passenger " + name + " leaves the car");

	  } finally {
		 lock.unlock();
	  }
   }

   //
   // Method called by the car
   //
   public void load() throws InterruptedException {
	  lock.lock();
	  try {
		 passagersCanBoard = true;
		 car_full.await();

		 System.out.println("\nThe car is full, let's go !\n "
			   + "------ Train departing ------->");

		 car_started.signalAll();
	  } finally {
		 lock.unlock();
	  }
   }

   public void unload() throws InterruptedException {
	  lock.lock();
	  try {
		 run_done.signalAll();

		 System.out.println("<------ Train finished -------\n"
			   + "The car is arrived, passangers can unboard !\n");

	  } finally {
		 lock.unlock();
	  }
   }

   public void animate(String name) throws InterruptedException {
	  lock.lock();

	  int waitTimeAnimations = RollerCoaster.random
		    .nextInt(Passager.MIN_WAIT_TIME_ANIMATE
		          + Passager.MAX_WAIT_TIME_ANIMATE
		          - Passager.MIN_WAIT_TIME_ANIMATE);
	  boolean runDone = run_done.await(waitTimeAnimations,
		    TimeUnit.MILLISECONDS);

	  if (!runDone && RollerCoaster.random.nextBoolean())
		 System.out.println(name
			   + Passager.animations[RollerCoaster.random
			         .nextInt(Passager.animations.length)]);

	  lock.unlock();
   }
}
