import java.util.Random;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Roller Coaster problem using Locks and Conditions
 * 
 * Monitor Class, this class is contains the methods called by the Passenger and
 * the Car. This class control every queues and allow threads to be
 * synchronized.
 * 
 * @author Kevin Python <kevin.python@hefr.ch>
 * @version 1.0
 * @since 2012-05-16
 * 
 */

public class Monitor {

   Random          random              = new Random();

   final Lock      lock                = new ReentrantLock();

   // The car wait that the car is full before running.
   final Condition car_full            = lock.newCondition();
   // Queue for waiting people who wants to board.
   final Condition car_free            = lock.newCondition();
   // Queue for people in the car, they're waiting the end of the run.
   final Condition run_done            = lock.newCondition();
   // Queue for people ho are waiting the start of the run before animate.
   final Condition car_started         = lock.newCondition();

   boolean         passengersCanBoard  = false;
   int             passengers_on_board = 0;

   //
   // Methods called by the passenger
   //
   public void board(String name) throws InterruptedException {
	  lock.lock();
	  try {

		 System.out.println("The passenger " + name + " wants to board");
		 while (!passengersCanBoard) {
			System.out.println("The passenger " + name + " has to wait !");
			car_free.await();
		 }

		 passengers_on_board++;
		 System.out.println("> The passenger (nb " + passengers_on_board
			   + "/" + Car.NB_OF_SEATS + ") " + name + " sits in the car");
		 if (passengers_on_board == Car.NB_OF_SEATS) {
			car_full.signal();
			passengersCanBoard = false;
		 }

		 car_started.await();

	  } finally {
		 lock.unlock();
	  }
   }

   public void unboard(String name) throws InterruptedException {
	  lock.lock();
	  try {
		 run_done.await();
		 passengers_on_board--;
		 if (passengers_on_board == 0) {
			car_free.signalAll();
		 }
		 System.out.println("< The passenger " + name + " leaves the car");

	  } finally {
		 lock.unlock();
	  }
   }

   //
   // Method called by the car
   //
   public void load() throws InterruptedException {
	  lock.lock();
	  try {
		 passengersCanBoard = true;
		 car_full.await();

		 System.out.println("\nThe car is full, let's go !\n "
			   + "------ Train departing ------->");

		 car_started.signalAll();
	  } finally {
		 lock.unlock();
	  }
   }

   public void unload() throws InterruptedException {
	  lock.lock();
	  try {
		 run_done.signalAll();

		 System.out.println("<------ Train finished -------\n"
			   + "The car is arrived, passangers can unboard !\n");

	  } finally {
		 lock.unlock();
	  }
   }

   public void animate(String name) throws InterruptedException {
	  lock.lock();

	  int waitTimeAnimations = RollerCoaster.random
		    .nextInt(Passenger.MIN_WAIT_TIME_ANIMATE
		          + Passenger.MAX_WAIT_TIME_ANIMATE
		          - Passenger.MIN_WAIT_TIME_ANIMATE);

	  boolean runDone = run_done.await(waitTimeAnimations,
		    TimeUnit.MILLISECONDS);
	  /*
	   * runDone == true -> the train is arrived, so no animation will performed
	   * runDone == false -> waitTimeAnimations expired , a random animation
	   * will performed
	   */

	  if (!runDone && RollerCoaster.random.nextBoolean())
		 System.out.println(name
			   + Passenger.animations[RollerCoaster.random
			         .nextInt(Passenger.animations.length)]);

	  lock.unlock();
   }
}
