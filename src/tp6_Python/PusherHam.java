package tp6_Python;

/**
 * The PusherHam class for the Sandwich Gourmet Problem
 * Concurrent Programming - TP6
 * 
 * @author      Kevin Python <kevin.python@hefr.ch>
 * @version     1.0                   
 * @since       2012-04-20
 * 
 * This class is the Ham pusher. A pusher keep track of the available 
 * ingredients, and signal the appropriate person. This pusher wakes up any 
 * time there is Ham on the table.
 */

public class PusherHam implements Runnable {
   
   @Override
   public void run() {
       while (true) {
    	  try {
    		 
    	     SandwichGourmets.ham.acquire();
    	     SandwichGourmets.mutex.acquire();
    	     
    	     if (SandwichGourmets.isButter){
    	    	SandwichGourmets.isButter = false;
    	    	SandwichGourmets.bakerSem.release();
    	     }
    	     else if (SandwichGourmets.isBread){
    	    	SandwichGourmets.isBread = false;
    	    	SandwichGourmets.milkmanSem.release();
    	     }
    	     else
    	    	SandwichGourmets.isHam = true;
    	     
    	     SandwichGourmets.mutex.release();
          } 
    	  	  
    	  catch (InterruptedException e) {
    	     e.printStackTrace();
          }	
       }
   }
}
