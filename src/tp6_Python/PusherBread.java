package tp6_Python;

/**
 * The PusherBread class for the Sandwich Gourmet Problem
 * Concurrent Programming - TP6
 * 
 * @author      Kevin Python <kevin.python@hefr.ch>
 * @version     1.0                   
 * @since       2012-04-20 
 * 
 * This class is the bread pusher. A pusher keep track of the available 
 * ingredients, and signal the appropriate person. This pusher wakes up any 
 * time there is Bread on the table.
 */

public class PusherBread implements Runnable {
   
   @Override
   public void run() {
       while (true) {
    	  try {
    		 
    	     SandwichGourmets.bread.acquire();
    	     SandwichGourmets.mutex.acquire();
    	     
    	     if (SandwichGourmets.isButter){
    	    	SandwichGourmets.isButter = false;
    	    	SandwichGourmets.butcherSem.release();
    	     }
    	     else if (SandwichGourmets.isHam){
    	    	SandwichGourmets.isHam = false;
    	    	SandwichGourmets.milkmanSem.release();
    	     }
    	     else
    	    	SandwichGourmets.isBread = true;
    	     
    	     SandwichGourmets.mutex.release();
          } 
    	  	  
    	  catch (InterruptedException e) {
    	     e.printStackTrace();
          }	
       }
   }
}
