package tp6_Python;

/**
 * The Milkman class for the Sandwich Gourmet Problem
 * Concurrent Programming - TP6
 * 
 * @author      Kevin Python <kevin.python@hefr.ch>
 * @version     1.0                   
 * @since       2012-04-20
 * 
 * This class represent the Milkman Thread. The Milkman wait for a signal from
 * the butter pusher, add is ingrendient, signal the grocer and eats is
 * sandwich.
 */

public class Milkman implements Runnable {

    @Override
    public void run() {
        while (true) {
                 
           try {
        	   // wait for bread and ham
	           SandwichGourmets.milkmanSem.acquire();
	           System.out.println("Milkman adds butter and makes sandwich");           
	           System.out.println("Milkman eats his sandwich");
	           SandwichGourmets.grocer_semaphore.release();
	           
           } catch (InterruptedException e) {	     
	           e.printStackTrace();
           }
        }
    }
}
