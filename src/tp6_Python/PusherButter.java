package tp6_Python;

/**
 * The PusherButter class for the Sandwich Gourmet Problem
 * Concurrent Programming - TP6
 * 
 * @author      Kevin Python <kevin.python@hefr.ch>
 * @version     1.0                   
 * @since       2012-04-20
 * 
 * This class is the butter pusher. A pusher keep track of the available 
 * ingredients, and signal the appropriate person. This pusher wakes up any 
 * time there is butter on the table.
 */

public class PusherButter implements Runnable {
   
   @Override
   public void run() {
       while (true) {
    	  try {
    		 
    	     SandwichGourmets.butter.acquire();
    	     SandwichGourmets.mutex.acquire();
    	     
    	     if (SandwichGourmets.isBread){
    	    	SandwichGourmets.isBread = false;
    	    	SandwichGourmets.butcherSem.release();
    	     }
    	     else if (SandwichGourmets.isHam){
    	    	SandwichGourmets.isHam = false;
    	    	SandwichGourmets.bakerSem.release();
    	     }
    	     else
    	    	SandwichGourmets.isButter = true;
    	     
    	     SandwichGourmets.mutex.release();
          } 
    	  	  
    	  catch (InterruptedException e) {
    	     e.printStackTrace();
          }	
       }
   }
}
