package tp6_Python;

/**
 * The Butcher class for the Sandwich Gourmet Problem
 * Concurrent Programming - TP6
 * 
 * @author      Kevin Python <kevin.python@hefr.ch>
 * @version     1.0                   
 * @since       2012-04-20 
 * 
 * This class represent the butcher Thread. The Butcher wait for a signal from
 * the ham pusher, add is ingrendient, signal the grocer and eats is
 * sandwich.
 */

public class Butcher implements Runnable {

    @Override
    public void run() {
        while (true) {
            
           try {
           	   // wait for bread and butter
	           SandwichGourmets.butcherSem.acquire();
	           System.out.println("Butcher adds ham and makes sandwich");            
	           System.out.println("Bucher eats his sandwich");
	           SandwichGourmets.grocer_semaphore.release();
	           
           } catch (InterruptedException e) {	     
	           e.printStackTrace();
           }          
        }
    }
}
