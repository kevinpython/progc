package tp6_Python;

/**
 * The Grocer class for the Sandwich Gourmet Problem
 * Concurrent Programming - TP6
 * 
 * @author      Jacques Supcik <jacques.supcik@hefr.ch>
 * @version     1.1                   
 * @since       2012-04-20
 *
 * THIS CLASS MUST BE USED AS-IT IN YOUR SOLUTION.
 * YOU ARE NOT ALLOWED TO MAKE ANY CHANGE IN THIS FILE!
 *
 */

import java.util.Random;

public class Grocer implements Runnable {

    static private Random randomGenerator = new Random();

    @Override
    public void run() {
        while (true) {
            try {
                SandwichGourmets.grocer_semaphore.acquire();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            switch (randomGenerator.nextInt(3)) {
            case 0:
                System.out.println("> bread + butter");
                SandwichGourmets.bread.release();
                SandwichGourmets.butter.release();         
                break;
            case 1:
                System.out.println("> bread + ham");
                SandwichGourmets.bread.release();
                SandwichGourmets.ham.release();
                break;
            case 2:
                System.out.println("> butter + ham");
                SandwichGourmets.butter.release();
                SandwichGourmets.ham.release();
                break;
            }
        }
    }
}
