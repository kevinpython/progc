package tp6_Python;

/**
 * The Main class for the Sandwich Gourmet Problem
 * Concurrent Programming - TP6
 * 
 * This class cointains the main program which create every threads
 * (grocer,baker,butcher,milkman) and start them.
 * 
 * @author      Kevin Python <kevin.python@hefr.ch>
 * @version     1.0                   
 * @since       2012-04-20
 *
 * Completion time: 1h40
 *
 * Honor Code: I pledge that this program represents my own
 * program code. I received help from no one in designing
 * and debugging my program.
 *
 */

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Semaphore;

public class SandwichGourmets {
    public static Semaphore grocer_semaphore  = new Semaphore(1, false);

    public static Semaphore bread             = new Semaphore(0, true);
    public static Semaphore ham               = new Semaphore(0, true);
    public static Semaphore butter            = new Semaphore(0, true);
    
    public static Semaphore bakerSem          = new Semaphore(0, true);
    public static Semaphore butcherSem        = new Semaphore(0, true);
    public static Semaphore milkmanSem        = new Semaphore(0, true);
    
    public static Semaphore mutex			  = new Semaphore(1, true);
    
    public static boolean   isBread 		  = false;
    public static boolean   isHam             = false;
    public static boolean   isButter          = false;
    
    
    public static ExecutorService threadExecutor = Executors
            .newCachedThreadPool();

    public static void main(String[] args) {
        Grocer grocer   = new Grocer();
        Baker baker     = new Baker();
        Butcher butcher = new Butcher();
        Milkman milkman = new Milkman();
        
        PusherBread pusherBread = new PusherBread();
        PusherButter pusherButter = new PusherButter();
        PusherHam pusherHam = new PusherHam();
        
        threadExecutor.execute(grocer);
        threadExecutor.execute(baker);
        threadExecutor.execute(butcher);
        threadExecutor.execute(milkman);
        threadExecutor.execute(pusherBread);
        threadExecutor.execute(pusherButter);
        threadExecutor.execute(pusherHam);
    }
}
