package tp6_Python;

/**
 * The Baker class for the Sandwich Gourmet Problem
 * Concurrent Programming - TP6
 * 
 * @author      Kevin Python <kevin.python@hefr.ch>
 * @version     1.0                   
 * @since       2012-04-20 
 * 
 * This class represent the baker Thread. The Baker wait for a signal from
 * the butter pusher, add is ingrendient, signal the grocer and eats is
 * sandwich.
 */

public class Baker implements Runnable {

    @Override
    public void run() {
        while (true) {
            
           try {
        	   // wait for ham and butter
	           SandwichGourmets.bakerSem.acquire();
	           System.out.println("Baker adds bread and makes sandwich");            
	           System.out.println("Baker eats his sandwich");
	           SandwichGourmets.grocer_semaphore.release();
	           
           } catch (InterruptedException e) {	     
	           e.printStackTrace();
           } 
        }
    }
}
