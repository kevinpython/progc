import java.io.File;
import java.io.FileNotFoundException;
import java.util.Random;
import java.util.Scanner;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Roller Coaster problem using Locks and Conditions Concurrent Programming -
 * TP9
 * 
 * This class cointains the main program which create every threads (car,
 * passengers) and start them.
 * 
 * @author Kevin Python <kevin.python@hefr.ch>
 * @version 1.0
 * @since 2012-05-16
 * 
 * Completion time: 4h30
 * 
 * Honor Code: I pledge that this program represents my own program code.
 * I received help from no one in designing and debugging my program.
 * 
 */

public class RollerCoaster {

   public static Random           random         = new Random();
   public static Monitor          monitor        = new Monitor();

   private static ExecutorService threadExecutor = Executors
	                                                   .newCachedThreadPool();

   final static int               NB_OF_CLIENTS  = 20;

   public static void main(String[] args) {
	  String name;
	  File inFile = new File("simpsonFamily.txt");
	  Scanner in;
	  try {
		 in = new Scanner(inFile);
		 Car car = new Car();
		 threadExecutor.execute(car);
		 Passenger passager[] = new Passenger[NB_OF_CLIENTS];
		 for (int i = 0; i < NB_OF_CLIENTS; i++) {
			name = in.nextLine();
			passager[i] = new Passenger(name);
			threadExecutor.execute(passager[i]);
		 }

	  } catch (FileNotFoundException e) {
		 e.printStackTrace();
	  }
   }
}
