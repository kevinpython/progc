package prog0;

/**
 * The main class for the dining philosopher problem. Concurrent Programming -
 * Assignment 2
 * 
 * @author Jacques Supcik <jacques.supcik@hefr.ch>
 * @version 1.0
 * @since 2012-03-05
 * 
 *        Completion time: 1h
 * 
 *        Honor Code: I pledge that this program represents my own program code.
 *        I received help from no one in designing and debugging my program.
 * 
 */

public class DiningPhilosophers {

  static final int   N              = 5;
  static Philosopher philosophers[] = new Philosopher[N];
  static Fork        forks[]        = new Fork[N];

  public static void main(String[] args) {
    Thread t;

    for (int i = 0; i < N; i++) {
      forks[i] = new Fork(i);
    }

    for (int i = 0; i < N-1; i++) {
      philosophers[i] = new Philosopher(i, forks[i], forks[(i + 1) % N]);
      t = new Thread(philosophers[i]);
      t.start();
    }
    philosophers[N] = new Philosopher(N, forks[N], forks[(N + 1) % N], false);
    t = new Thread(philosophers[N]);
    t.start();
  }
}