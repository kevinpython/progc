package prog0;

/**
 * A class to implement the philosopher in the dining philosopher problem.
 * Concurrent Programming - Assignment 2
 */

import java.util.Random;

public class Philosopher implements Runnable {

    static final int MAX_SLEEP_TIME = 2000;
    static final int MAX_EAT_TIME = 1000;

    int id = 0;
    Fork fork_left;
    Fork fork_right;
    boolean righthanded;
    
    static private Random randomGenerator = new Random();
    
    public Philosopher(int id, Fork fork_left, Fork fork_right) {
      this.id = id;
      this.fork_left = fork_left;
      this.fork_right = fork_right;
      this.righthanded = true;
  }
    
    public Philosopher(int id, Fork fork_left, Fork fork_right, boolean righthanded) {
        this.id = id;
        this.fork_left = fork_left;
        this.fork_right = fork_right;
        this.righthanded = righthanded;
    }

    public void run() {
        while (true) {
            System.out.println("Philosopher " + this.id + " sleeps...");
            try {
                Thread.sleep(randomGenerator.nextInt(MAX_SLEEP_TIME));
            } catch (InterruptedException e) {};
            System.out.println("Philosopher " + this.id + " is hungry");
            
            if (righthanded){
              fork_right.take(id);
              try {
	        Thread.sleep(2000);
              } catch (InterruptedException e) {
	        // TODO Auto-generated catch block
	        e.printStackTrace();
              }
              fork_left.take(id);
            }
            else{
              fork_left.take(id);
              try {
	        Thread.sleep(2000);
              } catch (InterruptedException e) {
	        // TODO Auto-generated catch block
	        e.printStackTrace();
              }
              fork_right.take(id);
            }
            System.out.println("Philosopher " + this.id + " eats...");
            try {
                Thread.sleep(randomGenerator.nextInt(MAX_EAT_TIME));
            } catch (InterruptedException e) {};
            fork_left.put(id);
            fork_right.put(id);
        }
    }
}