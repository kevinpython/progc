package prog0;

/**
 * A class to experiment with Producer/Consumer with Semaphores
 * Concurrent Programming - Assignment 2
 * 
 * @author      Jacques Supcik <jacques.supcik@hefr.ch>
 * @version     1.0                   
 * @since       2012-03-01
 *
 * Completion time: 1h
 *
 * Honor Code: I pledge that this program represents my own
 *   program code. I received help from no one in designing
 *   and debugging my program.
 *
 */

import java.util.concurrent.*;
import java.util.Random;

public class ProducerConsumer {

  final static int         N               = 8;                     // Buffer
								     // Size
  static int               buffer[]        = new int[N];

  static int               head            = 0;                     // head of
								     // the
								     // buffer
  static int               tail            = 0;                     // tail of
								     // the
								     // buffer

  static private Semaphore not_full        = new Semaphore(N, true);
  static private Semaphore not_empty       = new Semaphore(0, true);

  static private Semaphore mutexP          = new Semaphore(1, true);
  static private Semaphore mutexC          = new Semaphore(1, true);

  static private Random    randomGenerator = new Random();

  private static class Producer implements Runnable {
    String name = "Undef";

    Producer(String name) {
      this.name = name;
    }

    public void run() {
      System.out.println("Producer " + this.name + " Running");
      while (true) {
	int message = randomGenerator.nextInt(100);
	try {
	  Thread.sleep(10);
	} catch (InterruptedException e) {
	}
	;
	try {
	  not_full.acquire();
	} catch (InterruptedException e) {
	}
	;
	try {
	  mutexP.acquire();
	} catch (InterruptedException e) {
	}
	;
	System.out.println(this.name + " producing " + message);
	buffer[tail] = message;
	tail = (tail + 1) % N;
	mutexP.release();
	not_empty.release();
      }
    }
  }

  private static class Consumer implements Runnable {
    String name = "Undef";

    Consumer(String name) {
      this.name = name;
    }

    public void run() {
      System.out.println("Consumer " + this.name + " Running");
      while (true) {
	try {
	  not_empty.acquire();
	} catch (InterruptedException e) {
	}
	;
	try {
	  mutexC.acquire();
	} catch (InterruptedException e) {
	}
	;
	int message = buffer[head];
	System.out.println(this.name + " consuming " + message);
	head = (head + 1) % N;
	mutexC.release();
	not_full.release();
	try {
	  Thread.sleep(1000);
	} catch (InterruptedException e) {
	}
	;
      }
    }
  }

  public static void main(String args[]) {
    System.out.println("Main Programm");
    Thread p1 = new Thread(new Producer("P1"));
    p1.start();
    Thread c1 = new Thread(new Consumer("C1"));
    c1.start();
    Thread c2 = new Thread(new Consumer("C2"));
    c2.start();

    try {
      p1.join();
      c1.join();
      c2.join();
    } catch (InterruptedException e) {
      System.out.println("I wasn't done!");
    }

    System.out.println("Done");
  }
}