package prog0;

public class MatrixCalculator {
  
  static int[][] matrix_1 = new int[][]{
    {1,2,3},
    {4,5,6},
    {7,8,9},
  };
  
  static int[][] matrix_2 = new int[][]{
    {10,20,30},
    {40,50,60},
    {70,80,90},
  };

  static int[][] matrix_3 = new int[3][3];
  
  public static class MM extends Thread {
    
    int x;
    int y;
    
    public MM(int x, int y){
	this.x = x;
	this.y = y;
    }
    
    public void run() {
      System.out.println("Thread " + x + " " + y + " Started");
      
      for (int i = 0; i < matrix_1.length; i++){
	System.out.println("Thread " + x + " " + y + " Running");
	matrix_3[x][y] += matrix_1[x][i] * matrix_2[i][y];
      }
      Thread.yield(); //autoriser le processus d'�tre interrompu
      System.out.println("Thread " + x + " " + y + " Done");
    }
  }

  public static void main(String[] args) {
    MM[][] matrixThreads = new MM[3][3];
    for (int i = 0 ; i < matrixThreads.length; i++){
      for (int j = 0; j < matrixThreads.length; j++){
	matrixThreads[i][j] = new MM(i,j);
	matrixThreads[i][j].start();
      }
    }
    
    // Attendre que tout les processus soient termin�s
    for (Thread [] row: matrixThreads){
      for (Thread col : row){
	try{
	  col.join();
	} catch (InterruptedException e){
	  System.out.println("I wasn't done !");
	}
      }
    }
    
    // Affichage de la matrice r�sultante
    for(int [] row : matrix_3){
      for (int col : row){
	System.out.print(col + " ");
      }
      System.out.println();
    }
    System.out.println("DONE");
  }
}
